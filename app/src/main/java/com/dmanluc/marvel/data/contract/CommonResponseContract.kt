package com.dmanluc.marvel.data.contract

/**
 * Common output contracts for every JSON response from Marvel API
 *
 * @author Daniel Manrique Lucas
 */
data class Response<out E : MarvelCommonResponse>(
        val status: String?,
        val attributionText: String?,
        val data: Data<E>?)

data class Data<out E : MarvelCommonResponse>(
        val offset: Int?,
        val count: Int?,
        val results: List<E>?)

data class Thumbnail(
        val path: String?,
        val extension: String?)

data class ItemResponseList(
        val available: Int?,
        val returned: Int?,
        val collectionURI: String?,
        val items: List<ItemResponse>?)

data class ItemResponse(
        val name: String?,
        val type: String? = null,
        val role: String? = null,
        val resourceURI: String?)

data class TextObject(
        val type: String?,
        val language: String?,
        val text: String?)

data class UrlItemResponse(
        val type: String?,
        val url: String?)

data class PriceItemResponse(
        val type: String?,
        val price: Double?)

data class DateItemResponse(
        val type: String?,
        val date: String?)

interface MarvelCommonResponse
