package com.dmanluc.marvel.data.transformer

import com.dmanluc.marvel.data.contract.CharacterOutputContract
import com.dmanluc.marvel.data.contract.Response
import com.dmanluc.marvel.data.transformers.Transformer
import com.dmanluc.marvel.data.transformers.Transformer.Companion.DOT
import com.dmanluc.marvel.data.transformers.Transformer.Companion.IMAGE_BIG_SIZE
import com.dmanluc.marvel.data.transformers.Transformer.Companion.IMAGE_MEDIUM_SIZE
import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.SummaryItem
import com.dmanluc.marvel.domain.entity.SummaryList
import com.dmanluc.marvel.domain.entity.UrlItem

/**
 * Transformer to adapt character output contract from JSON response to Character´s domain entity of the app
 *
 * @author Daniel Manrique Lucas
 */
open class CharacterTransformer : Transformer<CharacterOutputContract, Character> {

    override fun transformContractToModel(outputContract: Response<CharacterOutputContract>): List<Character> =
            outputContract.data?.results?.map { c ->
                Character(
                        id = c.id ?: 0,
                        name = c.name ?: "",
                        description = c.description ?: "",
                        resourceURI = c.resourceURI ?: "",
                        modifiedDate = c.modified,
                        thumbnail = c.thumbnail?.let { it.path + IMAGE_MEDIUM_SIZE + DOT + it.extension } ?: "",
                        picture = c.thumbnail?.let { it.path + IMAGE_BIG_SIZE + DOT + it.extension } ?: "",
                        urls = c.urls?.map { urlItemResponse -> UrlItem(type = urlItemResponse.type ?: "", url = urlItemResponse.url ?: "") },
                        comicsSummary = SummaryList(
                                available = c.comics?.available ?: 0,
                                returned = c.comics?.returned ?: 0,
                                collectionURI = c.comics?.collectionURI ?: "",
                                items = c.comics?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        seriesSummary = SummaryList(
                                available = c.series?.available ?: 0,
                                returned = c.series?.returned ?: 0,
                                collectionURI = c.series?.collectionURI ?: "",
                                items = c.series?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        storiesSummary = SummaryList(
                                available = c.stories?.available ?: 0,
                                returned = c.stories?.returned ?: 0,
                                collectionURI = c.stories?.collectionURI ?: "",
                                items = c.stories?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList()),
                        eventsSummary = SummaryList(
                                available = c.events?.available ?: 0,
                                returned = c.events?.returned ?: 0,
                                collectionURI = c.events?.collectionURI ?: "",
                                items = c.events?.items?.map { com -> SummaryItem(name = com.name ?: "", resourceURI = com.resourceURI ?: "") }
                                        ?: emptyList())
                         )

            } ?: emptyList()

}
