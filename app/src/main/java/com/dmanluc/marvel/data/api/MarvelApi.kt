package com.dmanluc.marvel.data.api

import com.dmanluc.marvel.data.contract.CharacterOutputContract
import com.dmanluc.marvel.data.contract.ComicOutputContract
import com.dmanluc.marvel.data.contract.Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Marvel API to get characters and comics info
 *
 * @author Daniel Manrique Lucas
 */
interface MarvelApi {

    @GET("/v1/public/characters")
    fun getCharacters(
            @Query("apikey") apiKey: String,
            @Query("ts") time: String,
            @Query("hash") hash: String,
            @Query("orderBy") orderCriteria: String,
            @Query("offset") offset: Int): Single<Response<CharacterOutputContract>>

    @GET("/v1/public/comics")
    fun getComics(
            @Query("apikey") apiKey: String,
            @Query("ts") time: String,
            @Query("hash") hash: String,
            @Query("orderBy") orderCriteria: String,
            @Query("offset") offset: Int): Single<Response<ComicOutputContract>>

}
