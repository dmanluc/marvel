package com.dmanluc.marvel.data.transformers

import com.dmanluc.marvel.data.contract.MarvelCommonResponse
import com.dmanluc.marvel.data.contract.Response
import com.dmanluc.marvel.domain.entity.MarvelEntity

/**
 * Transformer interface to adapt an specific output contract from JSON response to the specific domain entity of the app
 *
 * @author Daniel Manrique Lucas
 */
interface Transformer<in R : MarvelCommonResponse, out T : MarvelEntity> {

    fun transformContractToModel(outputContract: Response<R>): List<T>

    companion object {
        const val DOT = "."
        const val IMAGE_MEDIUM_SIZE = "/standard_medium"
        const val IMAGE_BIG_SIZE = "/landscape_xlarge"
    }

}
