package com.dmanluc.marvel.data.contract

/**
 * Output contract for comic JSON response from Marvel API
 *
 * @author Daniel Manrique Lucas
 */
data class ComicOutputContract(
        val id: Int?,
        val digitalId: Int?,
        val title: String?,
        val issueNumber: Int?,
        val variantDescription: String?,
        val description: String?,
        val modified: String?,
        val isbn: String?,
        val upc: String?,
        val diamondCode: String?,
        val ean: String?,
        val issn: String?,
        val format: String?,
        val pageCount: Int?,
        val resourceURI: String?,
        val textObjects: List<TextObject>?,
        val urls: List<UrlItemResponse>?,
        val series: ItemResponse?,
        val variants: List<ItemResponse>?,
        val collections: List<ItemResponse>?,
        val collectedIssues: List<ItemResponse>?,
        val dates: List<DateItemResponse>?,
        val prices: List<PriceItemResponse>?,
        val images: List<Thumbnail>?,
        val creators: ItemResponseList?,
        val thumbnail: Thumbnail?,
        val characters: ItemResponseList?,
        val stories: ItemResponseList?,
        val events: ItemResponseList?) : MarvelCommonResponse
