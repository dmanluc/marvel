package com.dmanluc.marvel.data.repository

import com.dmanluc.marvel.BuildConfig
import com.dmanluc.marvel.data.api.MarvelApi
import com.dmanluc.marvel.data.transformer.CharacterTransformer
import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.Comic
import com.dmanluc.marvel.domain.repository.MarvelRepository
import com.dmanluc.marvel.utils.hashMd5
import io.reactivex.Single
import java.util.Date
import javax.inject.Inject

/**
 * Marvel repository implementation
 *
 * @author Daniel Manrique Lucas
 */
class MarvelRepositoryImpl
@Inject constructor(private val api: MarvelApi, private val transformer: CharacterTransformer) : MarvelRepository {

    override fun getCharacters(offset: Int): Single<List<Character>> {
        val apiKey = BuildConfig.MARVEL_API_KEY
        val apiSecret = BuildConfig.MARVEL_API_SECRET
        val date = Date().time.toString()
        val hash = "$date$apiSecret$apiKey".hashMd5()

        return api.getCharacters(apiKey,
                                 date,
                                 hash,
                                 "name",
                                 offset).map { transformer.transformContractToModel(it) }
    }

    override fun getComics(offset: Int): Single<List<Comic>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
