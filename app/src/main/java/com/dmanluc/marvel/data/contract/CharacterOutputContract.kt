package com.dmanluc.marvel.data.contract

import java.util.Date

/**
 * Output contract for character JSON response from Marvel API
 *
 * @author Daniel Manrique Lucas
 */
data class CharacterOutputContract(
        val id: Int?,
        val name: String?,
        val description: String?,
        val resourceURI: String?,
        val modified: Date?,
        val urls: List<UrlItemResponse>?,
        val thumbnail: Thumbnail?,
        val comics: ItemResponseList?,
        val series: ItemResponseList?,
        val stories: ItemResponseList?,
        val events: ItemResponseList?) : MarvelCommonResponse

