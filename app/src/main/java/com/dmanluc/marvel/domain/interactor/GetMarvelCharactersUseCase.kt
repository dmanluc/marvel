package com.dmanluc.marvel.domain.interactor

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.repository.MarvelRepository
import io.reactivex.Single

/**
 * Use case to get characters from Marvel API and a specific offset (for pagination purposes)
 *
 * @author Daniel Manrique Lucas
 */
class GetMarvelCharactersUseCase(private val repository: MarvelRepository) : UseCase<List<Character>, Int>() {

    override fun buildUseCaseObservable(offset: Int): Single<List<Character>> = repository.getCharacters(offset)

}
