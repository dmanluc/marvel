package com.dmanluc.marvel.domain.entity

import org.parceler.Parcel
import org.parceler.ParcelConstructor

/**
 * General data domain entity to save some fields of character´s data and comic´s data
 *
 * @author Daniel Manrique Lucas
 */
@Parcel(Parcel.Serialization.BEAN)
data class SummaryList @ParcelConstructor constructor(
        val available: Int,
        val returned: Int,
        val collectionURI: String,
        val items: List<SummaryItem>)

@Parcel(Parcel.Serialization.BEAN)
data class SummaryItem @ParcelConstructor constructor(
        val name: String,
        val resourceURI: String)

@Parcel(Parcel.Serialization.BEAN)
data class TextItem @ParcelConstructor constructor(
        val type: String,
        val language: String,
        val text: String)

@Parcel(Parcel.Serialization.BEAN)
data class UrlItem @ParcelConstructor constructor(
        val type: String,
        val url: String)

@Parcel(Parcel.Serialization.BEAN)
data class PriceItem @ParcelConstructor constructor(
        val type: String,
        val price: Double)

@Parcel(Parcel.Serialization.BEAN)
data class DateItem @ParcelConstructor constructor(
        val type: String,
        val date: String)
