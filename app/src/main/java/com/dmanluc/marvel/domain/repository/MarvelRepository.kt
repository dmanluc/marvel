package com.dmanluc.marvel.domain.repository

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.Comic
import io.reactivex.Single

/**
 * Marvel Repository interface
 *
 * @author Daniel Manrique Lucas
 */
interface MarvelRepository {

    fun getCharacters(offset: Int): Single<List<Character>>

    fun getComics(offset: Int): Single<List<Comic>>

}
