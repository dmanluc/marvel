package com.dmanluc.marvel.domain.entity

import org.parceler.Parcel
import org.parceler.ParcelConstructor
import java.util.Date

/**
 * Data domain entity to save character´s data
 *
 * @author Daniel Manrique Lucas
 */
@Parcel(Parcel.Serialization.BEAN)
data class Character @ParcelConstructor constructor(
        val id: Int,
        val name: String,
        val description: String,
        val modifiedDate: Date?,
        val resourceURI: String,
        val thumbnail: String,
        val urls: List<UrlItem>?,
        val picture: String,
        val comicsSummary: SummaryList,
        val seriesSummary: SummaryList,
        val storiesSummary: SummaryList,
        val eventsSummary: SummaryList) : MarvelEntity
