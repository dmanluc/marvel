package com.dmanluc.marvel.domain.interactor

import com.dmanluc.marvel.domain.entity.Comic
import com.dmanluc.marvel.domain.repository.MarvelRepository
import io.reactivex.Single

/**
 * Use case to get comics from Marvel API and a specific offset (for pagination purposes)
 *
 * @author Daniel Manrique Lucas
 */
class GetMarvelComicsUseCase(private val repository: MarvelRepository) : UseCase<List<Comic>, Int>() {

    override fun buildUseCaseObservable(offset: Int): Single<List<Comic>> = repository.getComics(offset)

}
