package com.dmanluc.marvel.utils

import java.security.MessageDigest

/**
 * Extension function to calculate the MD5 hash of a given string
 *
 * @author Daniel Manrique Lucas
 */
fun String.hashMd5(): String {
    val hexCharacters = "0123456789abcdef"
    val digest = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return digest.joinToString(separator = "",
                               transform = { a ->
                                   String(charArrayOf(hexCharacters[a.toInt() shr 4 and 0x0f],
                                                      hexCharacters[a.toInt() and 0x0f]))
                               })
}
