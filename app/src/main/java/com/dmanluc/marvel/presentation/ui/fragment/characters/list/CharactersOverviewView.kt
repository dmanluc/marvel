package com.dmanluc.marvel.presentation.ui.fragment.characters.list

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BaseView

/**
 * View contract of CharactersOverviewFragment
 *
 * @author Daniel Manrique Lucas
 */
interface CharactersOverviewView : BaseView {

    fun showCharacterList(characters: List<Character>, moreLoadingMode: Boolean)

    fun showLoadingProgress()

    fun hideLoadingProgress()

    fun showMarvelApiErrorMessage(errorMessage: String?)

}