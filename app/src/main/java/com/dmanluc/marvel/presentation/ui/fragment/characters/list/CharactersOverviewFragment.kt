package com.dmanluc.marvel.presentation.ui.fragment.characters.list

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.dmanluc.marvel.R
import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.adapter.CharactersOverviewAdapter
import com.dmanluc.marvel.presentation.base.BaseFragment
import com.dmanluc.marvel.presentation.di.component.DaggerCharactersOverviewFragmentComponent
import com.dmanluc.marvel.presentation.di.module.CharactersOverviewModule
import com.dmanluc.marvel.utils.EndlessRecyclerViewScrollListener
import kotlinx.android.synthetic.main.fragment_character_list.recycler
import kotlinx.android.synthetic.main.fragment_character_list.swipe_layout
import javax.inject.Inject

/**
 * Fragment to display the list of characters from Marvel API
 *
 * @author Daniel Manrique Lucas
 */
class CharactersOverviewFragment : BaseFragment<CharactersOverviewView, CharactersOverviewPresenterImpl, CharactersOverviewFragment.Callback>(),
                                   CharactersOverviewView, SwipeRefreshLayout.OnRefreshListener, CharactersOverviewAdapter.Listener {

    @Inject lateinit var internalPresenter: CharactersOverviewPresenterImpl
    @Inject lateinit var adapter: CharactersOverviewAdapter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    override val layoutId
        get() = R.layout.fragment_character_list
    override var screenTitle = ""
        get() = getString(R.string.app_name)
    override val presenter
        get() = internalPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLoadingProgress()
        presenter.loadCharacters(0, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureView()
    }

    override fun showBackArrow(): Boolean = false

    override fun provideDaggerDependency() {
        DaggerCharactersOverviewFragmentComponent.builder()
                .appComponent(appComponent)
                .charactersOverviewModule(CharactersOverviewModule(this))
                .build().inject(this)
    }

    override fun showCharacterList(characters: List<Character>, moreLoadingMode: Boolean) {
        adapter.setAdapterItems(characters, moreLoadingMode)
        if (swipe_layout.isRefreshing) swipe_layout.isRefreshing = false
    }

    override fun onCharacterSelected(character: Character) {
        callback?.onCharacterSelected(character)
    }

    override fun onRefresh() {
        presenter.loadCharacters(0, true)
        scrollListener.resetState()
    }

    override fun showLoadingProgress() {
        callback?.showLoadingProgress()
    }

    override fun hideLoadingProgress() {
        callback?.hideLoadingProgress()
    }

    override fun showMarvelApiErrorMessage(errorMessage: String?) {
        if (swipe_layout.isRefreshing) swipe_layout.isRefreshing = false
        errorMessage?.let { Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show() }
    }

    private fun configureView() {
        val linearLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = linearLayoutManager
        recycler.adapter = adapter
        recycler.setHasFixedSize(true)

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.loadCharacters(adapter.itemCount, false)
            }
        }

        recycler.addOnScrollListener(scrollListener)

        swipe_layout.setOnRefreshListener(this)
        swipe_layout.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3)
    }

    interface Callback : BaseCallback {

        fun showLoadingProgress()

        fun hideLoadingProgress()

        fun onCharacterSelected(character: Character)

    }

    companion object {

        fun newInstance() = CharactersOverviewFragment()

    }

}