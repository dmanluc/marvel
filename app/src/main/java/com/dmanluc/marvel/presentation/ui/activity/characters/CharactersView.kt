package com.dmanluc.marvel.presentation.ui.activity.characters

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BaseView

/**
 * View contract of CharactersActivity
 *
 * @author Daniel Manrique Lucas
 */
interface CharactersView : BaseView {

    fun showCharacterList()

    fun showCharacterDetails(character: Character)

    fun showConnectivityErrorDialog()

}
