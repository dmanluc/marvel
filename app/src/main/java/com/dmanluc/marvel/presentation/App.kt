package com.dmanluc.marvel.presentation

import android.app.Application
import android.content.Context
import com.dmanluc.marvel.BuildConfig
import com.dmanluc.marvel.presentation.di.component.AppComponent
import com.dmanluc.marvel.presentation.di.component.DaggerAppComponent
import com.dmanluc.marvel.presentation.di.module.AppModule

/**
 * Application class
 *
 * @author Daniel Manrique Lucas
 */
class App : Application() {

    companion object {

        lateinit var appComponent: AppComponent

    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this as Context, BuildConfig.MARVEL_BASE_URL))
                .build()
    }

}
