package com.dmanluc.marvel.presentation.ui.activity.characters

import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.dmanluc.marvel.R
import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BaseActivity
import com.dmanluc.marvel.presentation.di.component.DaggerCharactersActivityComponent
import com.dmanluc.marvel.presentation.ui.fragment.characters.detail.CharacterDetailFragment
import com.dmanluc.marvel.presentation.ui.fragment.characters.list.CharactersOverviewFragment
import javax.inject.Inject

/**
 * Main activity to handle navigation between characters list screen and character´s detail screen
 *
 * @author Daniel Manrique Lucas
 */
class CharactersActivity : BaseActivity<CharactersView, CharactersPresenterImpl>(), CharactersView, CharactersOverviewFragment.Callback {

    @Inject lateinit var internalPresenter: CharactersPresenterImpl

    override val layoutId
        get() = R.layout.base_activity_with_fragment
    override var screenTitle = ""
        get() = getString(R.string.app_name)
    override val presenter
        get() = internalPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isOnline()) navigateToCharacterListScreen() else presenter.handleOnlineConnectivityError()
    }

    override fun showBackArrow(): Boolean = false

    override fun provideDaggerDependency() {
        DaggerCharactersActivityComponent.builder()
                .appComponent(appComponent)
                .build().inject(this)
    }

    override fun showLoadingProgress() {
        showLoading()
    }

    override fun hideLoadingProgress() {
        hideLoading()
    }

    override fun showCharacterList() {
        switchFragment(CharactersOverviewFragment.newInstance(), false)
    }

    override fun showCharacterDetails(character: Character) {
        switchFragment(CharacterDetailFragment.newInstance(character), true)
    }

    override fun onCharacterSelected(character: Character) {
        presenter.goToCharacterDetailScreen(character)
    }

    override fun showConnectivityErrorDialog() {
        val alertBuilder = AlertDialog.Builder(this)

        alertBuilder.setTitle(getString(R.string.internet_connection_error_title))
        alertBuilder.setMessage(getString(R.string.internet_connection_error_text))
        alertBuilder.setCancelable(false)
        alertBuilder.setPositiveButton(getString(R.string.internet_connection_error_button_text).toUpperCase(),
                                       { _, _ -> System.exit(0) }
                                      )

        alertBuilder.create().show()
    }

    private fun navigateToCharacterListScreen() {
        presenter.goToCharacterListScreen()
    }

}
