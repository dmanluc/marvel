package com.dmanluc.marvel.presentation.di.component

import com.dmanluc.marvel.presentation.di.scope.FragmentScope
import com.dmanluc.marvel.presentation.ui.fragment.characters.detail.CharacterDetailFragment
import dagger.Component

/**
 * Component Interface for Dependency Injection (DI) in CharacterDetailFragment
 *
 * @author Daniel Manrique Lucas
 */
@FragmentScope
@Component(dependencies = arrayOf(AppComponent::class))
interface CharacterDetailFragmentComponent {

    fun inject(fragment: CharacterDetailFragment)

}