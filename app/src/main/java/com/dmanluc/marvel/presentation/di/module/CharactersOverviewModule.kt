package com.dmanluc.marvel.presentation.di.module

import com.dmanluc.marvel.data.api.MarvelApi
import com.dmanluc.marvel.data.repository.MarvelRepositoryImpl
import com.dmanluc.marvel.data.transformer.CharacterTransformer
import com.dmanluc.marvel.domain.interactor.GetMarvelCharactersUseCase
import com.dmanluc.marvel.presentation.adapter.CharactersOverviewAdapter
import com.dmanluc.marvel.presentation.di.scope.FragmentScope
import com.dmanluc.marvel.presentation.ui.fragment.characters.list.CharactersOverviewFragment
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Module class for providing dependencies
 *
 * @author Daniel Manrique Lucas
 * @since 13 Nov 2017
 */
@Module
class CharactersOverviewModule(private val fragment: CharactersOverviewFragment) {

    @FragmentScope
    @Provides
    fun provideMarvelService(retrofit: Retrofit): MarvelApi {
        return retrofit.create<MarvelApi>(MarvelApi::class.java)
    }

    @FragmentScope
    @Provides
    fun provideTransformer(): CharacterTransformer = CharacterTransformer()

    @FragmentScope
    @Provides
    fun provideMarvelRepository(service: MarvelApi, transformer: CharacterTransformer) = MarvelRepositoryImpl(service, transformer)


    @FragmentScope
    @Provides
    fun provideMarvelCharactersUseCase(repository: MarvelRepositoryImpl) = GetMarvelCharactersUseCase(repository)

    @FragmentScope
    @Provides
    fun provideCharacterAdapterListener(): CharactersOverviewAdapter.Listener {
        return fragment
    }

}