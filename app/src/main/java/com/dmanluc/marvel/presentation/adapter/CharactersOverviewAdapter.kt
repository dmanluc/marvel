package com.dmanluc.marvel.presentation.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.dmanluc.marvel.R
import com.dmanluc.marvel.domain.entity.Character
import kotlinx.android.synthetic.main.item_character_list.view.broken_image
import kotlinx.android.synthetic.main.item_character_list.view.character_image
import kotlinx.android.synthetic.main.item_character_list.view.character_name
import kotlinx.android.synthetic.main.item_character_list.view.progressBar
import java.net.URL
import javax.inject.Inject

/**
 * Adapter which prepares list of characters to be shown in the UI
 *
 * @author Daniel Manrique Lucas
 * @since 30 Dec 2017
 */
class CharactersOverviewAdapter @Inject constructor(
        private val context: Context, private val listener: Listener) : RecyclerView.Adapter<CharactersOverviewAdapter.ViewHolder>() {

    private val items: MutableList<Character> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_character_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount() = items.size

    fun setAdapterItems(characters: List<Character>, updateMode: Boolean) {
        if (!updateMode) {
            this.items.clear()
        }
        this.items.addAll(characters)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Character) {
            Glide.with(context)
                    .load(URL(item.picture))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.progressBar.visibility = View.GONE
                            itemView.broken_image.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
                                                     dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.broken_image.visibility = View.VISIBLE
                            itemView.progressBar.visibility = View.GONE
                            return false
                        }
                    })
                    .into(itemView.character_image)

            itemView.character_name.text = item.name
            itemView.setOnClickListener { listener.onCharacterSelected(item) }
        }
    }

    interface Listener {

        fun onCharacterSelected(character: Character)

    }

}
