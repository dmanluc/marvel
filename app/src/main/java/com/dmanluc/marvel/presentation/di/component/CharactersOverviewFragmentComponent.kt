package com.dmanluc.marvel.presentation.di.component

import com.dmanluc.marvel.data.api.MarvelApi
import com.dmanluc.marvel.data.repository.MarvelRepositoryImpl
import com.dmanluc.marvel.data.transformer.CharacterTransformer
import com.dmanluc.marvel.domain.interactor.GetMarvelCharactersUseCase
import com.dmanluc.marvel.presentation.adapter.CharactersOverviewAdapter
import com.dmanluc.marvel.presentation.di.module.CharactersOverviewModule
import com.dmanluc.marvel.presentation.di.scope.FragmentScope
import com.dmanluc.marvel.presentation.ui.fragment.characters.list.CharactersOverviewFragment
import dagger.Component

/**
 * Component Interface for Dependency Injection (DI) in CharactersOverviewFragment
 *
 * @author Daniel Manrique Lucas
 */
@FragmentScope
@Component(modules = arrayOf(CharactersOverviewModule::class), dependencies = arrayOf(AppComponent::class))
interface CharactersOverviewFragmentComponent {

    fun inject(fragment: CharactersOverviewFragment)

    fun provideMarvelService(): MarvelApi

    fun provideTransformer(): CharacterTransformer

    fun provideMarvelRepository(): MarvelRepositoryImpl

    fun provideMarvelCharactersUseCase(): GetMarvelCharactersUseCase

    fun provideCharacterAdapterListener(): CharactersOverviewAdapter.Listener

}