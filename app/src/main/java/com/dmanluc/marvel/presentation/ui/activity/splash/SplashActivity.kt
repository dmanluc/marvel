package com.dmanluc.marvel.presentation.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dmanluc.marvel.presentation.ui.activity.characters.CharactersActivity

/**
 * Initial splash activity
 *
 * @author Daniel Manrique Lucas
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, CharactersActivity::class.java)
        startActivity(intent)
        finish()
    }

}
