package com.dmanluc.marvel.presentation.di.component

import com.dmanluc.marvel.presentation.di.scope.ActivityScope
import com.dmanluc.marvel.presentation.ui.activity.characters.CharactersActivity
import dagger.Component

/**
 * Component Interface for Dependency Injection (DI) in CharactersActivity
 *
 * @author Daniel Manrique Lucas
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class))
interface CharactersActivityComponent {

    fun inject(activity: CharactersActivity)

}
