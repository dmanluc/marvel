package com.dmanluc.marvel.presentation.ui.fragment.characters.detail

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.dmanluc.marvel.R
import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.UrlItem
import com.dmanluc.marvel.presentation.base.BaseFragment
import com.dmanluc.marvel.presentation.di.component.DaggerCharacterDetailFragmentComponent
import com.dmanluc.marvel.presentation.ui.activity.characters.CharactersActivity
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_additional_info_comics_res
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_additional_info_detail_res
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_additional_info_wiki_res
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_broken_image
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_comics_number
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_description
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_events_number
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_image
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_series_number
import kotlinx.android.synthetic.main.fragment_character_detail.character_detail_stories_number
import kotlinx.android.synthetic.main.fragment_character_detail.progressBar
import org.parceler.Parcels
import java.net.URL
import javax.inject.Inject

/**
 * Fragment to display the details of the characters previously selected from the list of characters
 *
 * @author Daniel Manrique Lucas
 */
class CharacterDetailFragment : BaseFragment<CharacterDetailView, CharacterDetailPresenterImpl, BaseFragment.BaseCallback>(), CharacterDetailView {

    @Inject lateinit var internalPresenter: CharacterDetailPresenterImpl

    override val layoutId: Int
        get() = R.layout.fragment_character_detail
    override var screenTitle: String = ""
    override val presenter: CharacterDetailPresenterImpl
        get() = internalPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Parcels.unwrap<Character>(arguments?.getParcelable<Parcelable>(KEY_CHARACTER_SELECTED)).let {
            presenter.prepareCharacterDetailsView(it)
        }
    }

    override fun showBackArrow(): Boolean = true

    override fun provideDaggerDependency() {
        DaggerCharacterDetailFragmentComponent.builder()
                .appComponent(appComponent)
                .build().inject(this)
    }

    override fun showCharacterDetails(character: Character) {
        (activity as CharactersActivity).setActionBar(character.name, showBackArrow())

        Glide.with(context)
                .load(URL(character.picture))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.visibility = View.GONE
                        character_detail_broken_image.visibility = View.VISIBLE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
                                                 dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        character_detail_broken_image.visibility = View.GONE
                        progressBar.visibility = View.GONE
                        return false
                    }
                })
                .into(character_detail_image)

        if (character.description.isBlank()) {
            character_detail_description.visibility = GONE
        } else {
            character_detail_description.text = character.description
        }

        character_detail_comics_number.text = character.comicsSummary.available.toString()
        character_detail_events_number.text = character.eventsSummary.available.toString()
        character_detail_stories_number.text = character.storiesSummary.available.toString()
        character_detail_series_number.text = character.seriesSummary.available.toString()

        character.urls?.let { setUpListenerCharacterLinkResources(it) }
    }

    private fun setUpListenerCharacterLinkResources(urlItems: List<UrlItem>) {
        for (urlItem in urlItems) {
            when (urlItem.type) {
                CharacterResourceType.DETAIL.type -> {
                    character_detail_additional_info_detail_res.visibility = VISIBLE
                    character_detail_additional_info_detail_res.setOnClickListener {
                        urlItem.url.navigateToUrl(activity as Context)
                    }
                }
                CharacterResourceType.WIKI.type   -> {
                    character_detail_additional_info_wiki_res.visibility = VISIBLE
                    character_detail_additional_info_wiki_res.setOnClickListener {
                        urlItem.url.navigateToUrl(activity as Context)
                    }
                }
                CharacterResourceType.COMICS.type -> {
                    character_detail_additional_info_comics_res.visibility = VISIBLE
                    character_detail_additional_info_comics_res.setOnClickListener {
                        urlItem.url.navigateToUrl(activity as Context)
                    }
                }
            }
        }
    }

    private fun String.navigateToUrl(context: Context) {
        val prefixInsecureUrl = "http://"
        val prefixSecureUrl = "https://"
        var websiteUrl = this

        if (!this.startsWith(prefixSecureUrl) && !this.startsWith(prefixInsecureUrl)) {
            websiteUrl = prefixInsecureUrl + this
        }

        val openUrlIntent = Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl))

        openUrlIntent.resolveActivity(context.packageManager)?.let { startActivity(openUrlIntent) }
    }

    enum class CharacterResourceType(val type: String) {
        DETAIL("detail"),
        WIKI("wiki"),
        COMICS("comiclink")
    }

    companion object {

        const val KEY_CHARACTER_SELECTED = "key:bundle:character"

        fun newInstance(character: Character) = CharacterDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_CHARACTER_SELECTED, Parcels.wrap(character))
            }
        }
    }

}