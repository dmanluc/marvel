package com.dmanluc.marvel.presentation.ui.fragment.characters.list

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.interactor.BaseObserver
import com.dmanluc.marvel.domain.interactor.GetMarvelCharactersUseCase
import com.dmanluc.marvel.presentation.base.BasePresenter
import org.parceler.Parcel
import javax.inject.Inject

/**
 * Presenter of CharactersOverviewFragment
 *
 * @author Daniel Manrique Lucas
 */
class CharactersOverviewPresenterImpl
@Inject constructor(
        private val charactersUseCase: GetMarvelCharactersUseCase) : BasePresenter<CharactersOverviewView, CharactersOverviewPresenterImpl.State>
                                                                     () {

    fun loadCharacters(offset: Int, refreshMode: Boolean) {
        if (!refreshMode && isViewAttached()) view?.showLoadingProgress()

        charactersUseCase.execute(object : BaseObserver<List<Character>>() {
            override fun onSuccess(t: List<Character>) {
                super.onSuccess(t)
                if (!refreshMode) view?.hideLoadingProgress()
                view?.showCharacterList(t, offset > 0)
            }

            override fun onErrorMessage(errorMessage: String?) {
                super.onErrorMessage(errorMessage)
                view?.hideLoadingProgress()
                view?.showMarvelApiErrorMessage(errorMessage)
            }
        }, offset)
    }

    override fun newState(): State = State()

    @Parcel
    class State : BasePresenter.State
}