package com.dmanluc.marvel.presentation.di.scope

import javax.inject.Scope

/**
 * Custom Scope for DI
 *
 * @author Daniel Manrique Lucas
 * @since 13 Nov 2017
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
