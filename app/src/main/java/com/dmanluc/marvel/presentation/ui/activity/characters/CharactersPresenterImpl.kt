package com.dmanluc.marvel.presentation.ui.activity.characters

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BasePresenter
import org.parceler.Parcel
import javax.inject.Inject

/**
 * Presenter of CharactersActivity
 *
 * @author Daniel Manrique Lucas
 */
class CharactersPresenterImpl @Inject constructor() : BasePresenter<CharactersView, CharactersPresenterImpl.State>() {

    fun goToCharacterListScreen() {
        if (isViewAttached()) view?.showCharacterList()
    }

    fun goToCharacterDetailScreen(character: Character) {
        if (isViewAttached()) view?.showCharacterDetails(character)
    }

    fun handleOnlineConnectivityError() {
        if (isViewAttached()) view?.showConnectivityErrorDialog()
    }

    override fun newState(): State = State()

    @Parcel
    class State : BasePresenter.State
}
