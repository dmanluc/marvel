package com.dmanluc.marvel.presentation.ui.fragment.characters.detail

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BaseView

/**
 * View contract of CharacterDetailFragment
 *
 * @author Daniel Manrique Lucas
 */
interface CharacterDetailView : BaseView {

    fun showCharacterDetails(character: Character)

}