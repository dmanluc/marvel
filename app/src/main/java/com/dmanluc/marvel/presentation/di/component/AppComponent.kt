package com.dmanluc.marvel.presentation.di.component

import android.content.Context
import com.dmanluc.marvel.presentation.di.module.AppModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * App Component Interface for Dependency Injection (DI)
 *
 * @author Daniel Manrique Lucas
 * @since 13 Nov 2017
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun provideApplication(): Context

    fun provideRetrofit(): Retrofit

}
