package com.dmanluc.marvel.presentation.ui.fragment.characters.detail

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.presentation.base.BasePresenter
import org.parceler.Parcel
import javax.inject.Inject

/**
 * Presenter of CharacterDetailFragment
 *
 * @author Daniel Manrique Lucas
 */
class CharacterDetailPresenterImpl
@Inject constructor() : BasePresenter<CharacterDetailView, CharacterDetailPresenterImpl.State>() {

    fun prepareCharacterDetailsView(character: Character) {
        view?.showCharacterDetails(character)
    }

    override fun newState(): State = State()

    @Parcel
    class State : BasePresenter.State
}