package com.dmanluc.marvel.presentation.ui.fragment.characters.detail

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.SummaryList
import com.dmanluc.marvel.domain.entity.UrlItem
import org.easymock.EasyMock
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.util.Date

/**
 * Unit test of CharacterDetailFragment´s presenter implementation
 *
 * @author Daniel Manrique Lucas
 */
@PrepareForTest()
@RunWith(PowerMockRunner::class)
class CharacterDetailPresenterImplTest {

    private lateinit var view: CharacterDetailView
    private lateinit var presenter: CharacterDetailPresenterImpl
    private lateinit var state: CharacterDetailPresenterImpl.State

    @Before
    fun setUp() {
        view = PowerMock.createMock(CharacterDetailView::class.java)
        presenter = CharacterDetailPresenterImpl()
        state = CharacterDetailPresenterImpl.State()
    }

    @After
    fun cleanUp() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun prepareCharacterDetailsView_showCharacterDetails() {
        val character = mockCharacter(3)
        view.showCharacterDetails(character)
        EasyMock.expect(presenter.isViewAttached()).anyTimes()

        PowerMock.replayAll()

        presenter.attachView(view)
        presenter.prepareCharacterDetailsView(character)
    }

    private fun mockCharacter(id: Int) = Character(
            id = id,
            name = "name",
            description = "description",
            modifiedDate = Date(),
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            urls = arrayListOf(getUrlItem()),
            comicsSummary = getSummaryList(),
            seriesSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())

    private fun getUrlItem() = UrlItem(
            type = "detail",
            url = "https://marvel.com")

}