package com.dmanluc.marvel.presentation.ui.fragment.characters.list

import com.dmanluc.marvel.data.api.MarvelApi
import com.dmanluc.marvel.data.contract.CharacterOutputContract
import com.dmanluc.marvel.data.contract.Data
import com.dmanluc.marvel.data.contract.ItemResponse
import com.dmanluc.marvel.data.contract.ItemResponseList
import com.dmanluc.marvel.data.contract.Response
import com.dmanluc.marvel.data.contract.Thumbnail
import com.dmanluc.marvel.data.contract.UrlItemResponse
import com.dmanluc.marvel.data.repository.MarvelRepositoryImpl
import com.dmanluc.marvel.data.transformer.CharacterTransformer
import com.dmanluc.marvel.domain.interactor.GetMarvelCharactersUseCase
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.model.Statement
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.core.classloader.annotations.PrepareForTest
import java.util.Calendar
import java.util.Date
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

/**
 * Unit test of CharactersOverviewFragment´s presenter implementation
 *
 * @author Daniel Manrique Lucas
 */
@PrepareForTest(CharacterTransformer::class)
@RunWith(MockitoJUnitRunner::class)
class CharactersOverviewPresenterImplTest {

    @Mock private lateinit var view: CharactersOverviewView
    private lateinit var presenter: CharactersOverviewPresenterImpl
    private lateinit var state: CharactersOverviewPresenterImpl.State
    @Mock private lateinit var service: MarvelApi
    private lateinit var transformer: CharacterTransformer
    private lateinit var repository: MarvelRepositoryImpl
    private lateinit var interactor: GetMarvelCharactersUseCase

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        transformer = CharacterTransformer()
        repository = MarvelRepositoryImpl(service, transformer)
        interactor = GetMarvelCharactersUseCase(repository)
        presenter = CharactersOverviewPresenterImpl(interactor)
        state = CharactersOverviewPresenterImpl.State()
        presenter.attachView(view)
    }

    @Test
    fun loadCharacters_withoutRefreshMode_success() {
        val offset = 0
        val mockResponse = mockResponseCharacterListData()

        `when`(service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt())).thenReturn(Single.just(mockResponse))

        val result = service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt()).test().assertComplete()

        presenter.loadCharacters(offset, false)

        verify(view, times(1)).showLoadingProgress()
        verify(view, times(1)).hideLoadingProgress()
        verify(view, times(1)).showCharacterList(transformer.transformContractToModel(result.values()[0]), false)
    }

    @Test
    fun loadCharacters_withRefreshMode_success() {
        val offset = 20
        val mockResponse = mockResponseCharacterListData()

        `when`(service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt())).thenReturn(Single.just(mockResponse))

        val result = service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt()).test().assertComplete()

        presenter.loadCharacters(offset, true)

        verify(view, times(1)).showCharacterList(transformer.transformContractToModel(result.values()[0]), true)
    }

    @Test
    fun loadCharacters_error() {
        val offset = 20
        val errorException = Throwable("Error")

        `when`(service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt())).thenReturn(Single.error(errorException))

        val result = service.getCharacters(anyString(), anyString(), anyString(), anyString(), anyInt()).test().assertError(errorException)

        presenter.loadCharacters(offset, false)

        verify(view, times(1)).showLoadingProgress()
        verify(view, times(1)).hideLoadingProgress()
        verify(view, times(1)).showMarvelApiErrorMessage("Error")
    }

    private fun mockResponseCharacterListData(): Response<CharacterOutputContract> {
        val listItems = listOf<CharacterOutputContract>(mockCharacter(1), mockCharacter(2))

        val data = Data<CharacterOutputContract>(0, 20, listItems)
        return Response<CharacterOutputContract>("status", "att", data)
    }

    private fun mockCharacter(id: Int) = CharacterOutputContract(
            id = id,
            name = "name",
            description = "description",
            modified = Date(Calendar.getInstance().timeInMillis),
            resourceURI = "resourceURI",
            thumbnail = getThumbnailItem(),
            urls = arrayListOf(getUrlItem()),
            comics = getItemResponseList(),
            series = getItemResponseList(),
            stories = getItemResponseList(),
            events = getItemResponseList())


    private fun getThumbnailItem() = Thumbnail(
            path = "path",
            extension = "jpg")

    private fun getItemResponseList() = ItemResponseList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = getItemResponse())

    private fun getItemResponse() = arrayListOf<ItemResponse>(ItemResponse("name", "type", "role", "uri"))

    private fun getUrlItem() = UrlItemResponse(
            type = "detail",
            url = "https://marvel.com")

    class RxImmediateSchedulerRule : TestRule {
        private val immediate = object : Scheduler() {
            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Scheduler.Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }

        override fun apply(base: Statement, description: Description): Statement {
            return object : Statement() {
                @Throws(Throwable::class)
                override fun evaluate() {
                    RxJavaPlugins.setInitIoSchedulerHandler { immediate }
                    RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
                    RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
                    RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
                    RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }

                    try {
                        base.evaluate()
                    } finally {
                        RxJavaPlugins.reset()
                        RxAndroidPlugins.reset()
                    }
                }
            }
        }
    }
}