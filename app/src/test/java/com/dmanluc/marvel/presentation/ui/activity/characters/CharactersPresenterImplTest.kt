package com.dmanluc.marvel.presentation.ui.activity.characters

import com.dmanluc.marvel.domain.entity.Character
import com.dmanluc.marvel.domain.entity.SummaryList
import com.dmanluc.marvel.domain.entity.UrlItem
import org.easymock.EasyMock
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.easymock.PowerMock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import java.util.Calendar
import java.util.Date

/**
 * Unit test of CharactersActivity´s presenter implementation
 *
 * @author Daniel Manrique Lucas
 */
@PrepareForTest()
@RunWith(PowerMockRunner::class)
class CharactersPresenterImplTest {

    private lateinit var view: CharactersView
    private lateinit var presenter: CharactersPresenterImpl
    private lateinit var state: CharactersPresenterImpl.State

    @Before
    fun setUp() {
        view = PowerMock.createMock(CharactersView::class.java)
        presenter = CharactersPresenterImpl()
        state = CharactersPresenterImpl.State()
    }

    @After
    fun cleanUp() {
        PowerMock.verifyAll()
        PowerMock.resetAll()
    }

    @Test
    fun goToCharacterListScreen_navigateToCharacterListScreen() {
        view.showCharacterList()
        EasyMock.expect(presenter.isViewAttached()).anyTimes()

        PowerMock.replayAll()

        presenter.attachView(view)
        presenter.goToCharacterListScreen()
    }

    @Test
    fun goToCharacterDetailScreen_navigateToCharacterDetailScreen() {
        val character = mockCharacter(1)
        view.showCharacterDetails(character)
        EasyMock.expect(presenter.isViewAttached()).anyTimes()

        PowerMock.replayAll()

        presenter.attachView(view)
        presenter.goToCharacterDetailScreen(character)
    }

    @Test
    fun handleOfflineConnectivity_showConnectivityError() {
        view.showConnectivityErrorDialog()
        EasyMock.expect(presenter.isViewAttached()).anyTimes()

        PowerMock.replayAll()

        presenter.attachView(view)
        presenter.handleOnlineConnectivityError()
    }

    private fun mockCharacter(id: Int) = Character(
            id = id,
            name = "name",
            description = "description",
            modifiedDate = Date(Calendar.getInstance().timeInMillis),
            resourceURI = "resourceURI",
            thumbnail = "thumbnail",
            picture = "picture",
            urls = arrayListOf(getUrlItem()),
            comicsSummary = getSummaryList(),
            seriesSummary = getSummaryList(),
            storiesSummary = getSummaryList(),
            eventsSummary = getSummaryList())

    private fun getSummaryList() = SummaryList(
            available = 0,
            returned = 0,
            collectionURI = "collectionURI",
            items = emptyList())

    private fun getUrlItem() = UrlItem(
            type = "detail",
            url = "https://marvel.com")

}